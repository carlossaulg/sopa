package Negocio;

import Vista.Interface_Sopa;
import java.io.*;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;


public class SopaDeLetras
{
    // instance variables - replace the example below with your own
    private char sopas[][];
    Interface_Sopa Texto = new Interface_Sopa();
//
//    /**
//     * Constructor for objects of class SopaDeLetras
//     */
//    public SopaDeLetras()
//    {
//        
//    }
//
//    
//    
//    
//    
//    
//    public SopaDeLetras(String palabras) throws Exception
//    {
//        if(palabras==null || palabras.isEmpty())
//        {
//            throw new Exception("Error no se puede crear la matriz de char para la sopa de letras");
//        }
//        
//     //Crear la matriz con las correspondientes filas:
//     
//     String palabras2[]=palabras.split(",");
//     this.sopas=new char[palabras2.length][];
//     int i=0;
//     for(String palabraX:palabras2)
//     {
//         //Creando las columnas de la fila i
//         this.sopas[i]=new char[palabraX.length()];
//         pasar(palabraX,this.sopas[i]);
//         i++;
//        
//     }
//     
//     
//    }
//    
//    private void pasar (String palabra, char fila[])
//    {
//    
//        for(int j=0;j<palabra.length();j++)
//        {
//            fila[j]=palabra.charAt(j);
//        }
//    }
//    
//    
//    public String toString()
//    {
//    String msg="";
//    for(int i=0;i<this.sopas.length;i++)
//    {
//        for (int j=0;j<this.sopas[i].length;j++)
//        {
//            msg+=this.sopas[i][j]+"\t";
//        }
//        
//        msg+="\n";
//        
//    }
//    return msg;
//    }
//    
//    
//    public String toString2()
//    {
//    String msg="";
//    for(char filas[]:this.sopas)
//    {
//        for (char dato :filas)
//        {
//            msg+=dato+"\t";
//        }
//        
//        msg+="\n";
//        
//    }
//    return msg;
//    }
    
    public void leerExcel(String Nombre) throws Exception {
        
        HSSFWorkbook Excel = new HSSFWorkbook(new FileInputStream(Nombre));
        HSSFSheet hoja = Excel.getSheetAt(0);
        
        int Filas = hoja.getLastRowNum()+1;
        int Columnas = 0;
        
        this.sopas = new char[Filas][];
        for (int i = 0; i <Filas; i++) {
            HSSFRow fil = hoja.getRow(i);
            Columnas =fil.getLastCellNum();
            this.sopas[i] = new char[Columnas];
        for(int j=0;j<Columnas;j++)    
        {
            this.sopas[i][j] = fil.getCell(j).getStringCellValue().charAt(0);
            Texto.jTextArea1.setText(sopas+"\t");
        }        
       }
    }
    
    public boolean esCuadrada()
    {
        boolean cuadrada = true;
        for(int i=0; i<sopas.length;i++)
        {
            if(sopas[i].length!=sopas.length)
            {return false;}    
        }
        return cuadrada;
    }
    
    
     public boolean esDispersa() throws Exception{
        if(sopas==null)
        {
            throw new Exception("la matriz esta vacia");
        }
        
        int j=1;
        boolean dispersa=true;
        int contador=0;
        for(int i=0;i+1<sopas.length;i++){
            if(j!=sopas.length){
                if(sopas[i].length!=sopas[j].length)
                {contador++;}
            }j++;
        }
        dispersa=contador!=0;
    
        return dispersa;
    }
    
    public boolean esRectangular() throws Exception{
        if(sopas==null)
        {
            throw new Exception("La matriz esta vacia");
        }
        
        if(!esDispersa()&&!esCuadrada())
        {
            return true;
        }
        else
        {return false;}
    }

    public int getContar(String palabra)
    {
        int contador = 0; 
        String cadena = "";   
        for(int i=0; i<sopas.length;i++)
        {           

         for(int j=0; j<sopas.length;j++)
         {
            cadena += sopas[i][j];
            if(cadena== palabra)
            {
                contador++;
            }            
         } 
         System.out.println(cadena+" "+contador);
        }     
        return contador;
    }
    
    
    /*
        debe ser cuadrada sopas
       */
    public char []getDiagonalPrincipal() throws Exception
    {
        return  null;
    }

    //Start GetterSetterExtension Source Code
    /**GET Method Propertie sopas*/
    public char[][] getSopas(){
        return this.sopas;
    }//end method getSopas

    //End GetterSetterExtension Source Code
//!
}
